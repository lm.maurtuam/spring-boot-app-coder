package pruebaGitLab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaGitLabApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaGitLabApplication.class, args);
	}

}
